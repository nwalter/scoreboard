package org.bitbucket.nwalter.scoreboard.api.persistence.registration;

import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete;
import org.bitbucket.nwalter.scoreboard.api.competition.Competition;
import org.bitbucket.nwalter.scoreboard.api.persistence.athlete.PersistentAthlete;
import org.bitbucket.nwalter.scoreboard.api.persistence.competition.PersistentCompetition;
import org.bitbucket.nwalter.scoreboard.api.registration.AthleteRegistration;
import org.bitbucket.nwalter.scoreboard.api.registration.AthleteRegistrationManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersistentAthleteRegistrationManager implements AthleteRegistrationManager {

  @Autowired
  private AthleteRegistrationRepository repository;

  @Override
  public AthleteRegistration createRegistration(final Athlete athlete, final Competition competition) {
    final PersistentAthleteRegistration registration =
        new PersistentAthleteRegistration((PersistentAthlete) athlete, (PersistentCompetition) competition);
    competition.addRegistration(registration);
    return this.repository.save(registration);
  }

}
