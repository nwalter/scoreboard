package org.bitbucket.nwalter.scoreboard.api.persistence.competition;

import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete;
import org.bitbucket.nwalter.scoreboard.api.competition.Competition;
import org.bitbucket.nwalter.scoreboard.api.event.Event;
import org.bitbucket.nwalter.scoreboard.api.persistence.event.PersistentEvent;
import org.bitbucket.nwalter.scoreboard.api.persistence.registration.PersistentAthleteRegistration;
import org.bitbucket.nwalter.scoreboard.api.registration.AthleteRegistration;
import org.bitbucket.nwalter.scoreboard.utils.IterableUtils;

import com.google.common.collect.FluentIterable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "name")})
public class PersistentCompetition implements Competition {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String name;

  @OneToMany(
      fetch = FetchType.EAGER,
      mappedBy = "competition")
  private final Collection<PersistentAthleteRegistration> athletes = new ArrayList<>();

  @OneToMany(mappedBy = "competition")
  private final List<PersistentEvent> events = new ArrayList<>();

  protected PersistentCompetition() {}

  protected PersistentCompetition(final String name) {
    super();
    this.name = name;
  }

  @Override
  public Long getId() {
    return this.id;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public List<Event> getEvents() {
    return IterableUtils.castList(this.events);
  }

  @Override
  public Collection<AthleteRegistration> getAthletes() {
    return IterableUtils.castCollection(this.athletes);
  }

  @Override
  public void addRegistration(final AthleteRegistration registration) {
    this.athletes.add((PersistentAthleteRegistration) registration);
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public boolean isRegistered(final Athlete athlete) {
    return FluentIterable.from(this.athletes).transform(reg -> {
      return reg.getAthlete();
    }).contains(athlete);
  }
}
