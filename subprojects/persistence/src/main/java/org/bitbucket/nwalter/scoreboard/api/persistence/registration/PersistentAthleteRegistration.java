package org.bitbucket.nwalter.scoreboard.api.persistence.registration;

import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete;
import org.bitbucket.nwalter.scoreboard.api.competition.Competition;
import org.bitbucket.nwalter.scoreboard.api.persistence.athlete.PersistentAthlete;
import org.bitbucket.nwalter.scoreboard.api.persistence.competition.PersistentCompetition;
import org.bitbucket.nwalter.scoreboard.api.registration.AthleteRegistration;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {
    "athlete_id", "competition_id"
}))
public class PersistentAthleteRegistration implements AthleteRegistration {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "athlete_id")
  private PersistentAthlete athlete;

  @ManyToOne
  @JoinColumn(name = "competition_id")
  private PersistentCompetition competition;

  protected PersistentAthleteRegistration() {}

  public PersistentAthleteRegistration(final PersistentAthlete athlete, final PersistentCompetition competition) {
    this.athlete = athlete;
    this.competition = competition;
  }

  @Override
  public Long getId() {
    return this.id;
  }

  @Override
  public Athlete getAthlete() {
    return this.athlete;
  }

  @Override
  public Competition getCompetition() {
    return this.competition;
  }

}
