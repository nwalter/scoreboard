package org.bitbucket.nwalter.scoreboard.api.persistence.registration;

import org.springframework.data.repository.CrudRepository;

public interface AthleteRegistrationRepository extends
    CrudRepository<PersistentAthleteRegistration, Long> {

}
