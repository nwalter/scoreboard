package org.bitbucket.nwalter.scoreboard.api.persistence.athlete;

import org.bitbucket.nwalter.scoreboard.api.persistence.CrudNamedObjectRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AthleteRepository extends CrudNamedObjectRepository<PersistentAthlete, Long> {

  @Query(value = "SELECT * FROM athletes WHERE firstName LIKE %:name% OR name LIKE %:name%",
      nativeQuery = true)
  List<PersistentAthlete> searchByName(@Param("name") String name);

}
