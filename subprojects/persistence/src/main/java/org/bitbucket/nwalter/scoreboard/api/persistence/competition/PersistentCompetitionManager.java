package org.bitbucket.nwalter.scoreboard.api.persistence.competition;

import org.bitbucket.nwalter.scoreboard.api.competition.Competition;
import org.bitbucket.nwalter.scoreboard.api.competition.CompetitionManager;
import org.bitbucket.nwalter.scoreboard.api.persistence.PersistentNamedObjectManager;
import org.bitbucket.nwalter.scoreboard.utils.IterableUtils;

import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.NoSuchElementException;

@Component
public class PersistentCompetitionManager extends PersistentNamedObjectManager<Competition, PersistentCompetition>
implements CompetitionManager {

  private final CompetitionRepository repository;

  @Autowired
  public PersistentCompetitionManager(final CompetitionRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  public Competition getByName(final String name) {
    final List<PersistentCompetition> competitions = this.repository.findByName(name);
    if (competitions.isEmpty()) {
      throw new NoSuchElementException(name);
    }

    return Iterables.getOnlyElement(competitions);
  }

  @Override
  public Optional<Competition> findByName(final String name) {
    final List<PersistentCompetition> competitions = this.repository.findByName(name);
    return IterableUtils.<Competition>getOnlyElement(competitions);
  }

  @Override
  public Iterable<Competition> getAll() {
    return IterableUtils.coerce(this.repository.findAll());
  }

  @Override
  public Competition create(final String name) {
    if (this.findByName(name).isPresent()) {
      throw new IllegalArgumentException("Competition with name " + name + " already defined.");
    }
    final PersistentCompetition newCompetition = new PersistentCompetition(name);
    return this.repository.save(newCompetition);

  }

  @Override
  public void save(final Competition competition) {
    this.repository.save((PersistentCompetition) competition);
  }

  @Override
  public Competition getById(final long id) {
    return this.repository.findOne(id);
  }

}
