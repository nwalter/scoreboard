package org.bitbucket.nwalter.scoreboard.api.persistence.athlete;

import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete;
import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete.Gender;
import org.bitbucket.nwalter.scoreboard.api.athlete.AthleteManager;
import org.bitbucket.nwalter.scoreboard.api.persistence.PersistentNamedObjectManager;
import org.bitbucket.nwalter.scoreboard.utils.IterableUtils;

import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

@Component
public class PersistentAthleteManager extends PersistentNamedObjectManager<Athlete, PersistentAthlete>
implements AthleteManager {

  private final AthleteRepository repository;

  @Autowired
  public PersistentAthleteManager(final AthleteRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  public Athlete getByName(final String name) {
    final List<PersistentAthlete> athletes = this.repository.findByName(name);
    if (Iterables.isEmpty(athletes)) {
      throw new NoSuchElementException(name);
    }
    return Iterables.getOnlyElement(athletes);
  }

  @Override
  public Optional<Athlete> findByName(final String name) {
    final List<PersistentAthlete> athlete = this.repository.findByName(name);
    return IterableUtils.<Athlete>getOnlyElement(athlete);
  }

  @Override
  public Iterable<Athlete> getAll() {
    return IterableUtils.coerce(this.repository.findAll());
  }

  @Override
  public Athlete getById(final Long id) {
    return this.repository.findOne(id);
  }

  @Override
  public Athlete create(final String name, final String firstName, final LocalDate birthday, final Gender gender) {
    final PersistentAthlete athlete = new PersistentAthlete(name, firstName, birthday, gender);
    return this.save(athlete);
  }

  @Override
  public Athlete save(final Athlete athlete) {
    return this.repository.save((PersistentAthlete) athlete);
  }

  @Override
  public List<Athlete> searchByName(final String query) {
    return IterableUtils.castList(this.repository.searchByName(query));
  }
}
