package org.bitbucket.nwalter.scoreboard.api.persistence.athlete;

import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete;
import org.bitbucket.nwalter.scoreboard.api.persistence.registration.PersistentAthleteRegistration;
import org.bitbucket.nwalter.scoreboard.api.registration.AthleteRegistration;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "athletes")
public class PersistentAthlete implements Athlete {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String name;
  private String firstname;
  private LocalDate birthday;
  private Gender gender;

  @OneToMany(mappedBy = "athlete",
      targetEntity = PersistentAthleteRegistration.class)
  private final List<AthleteRegistration> registrations = new ArrayList<AthleteRegistration>();

  protected PersistentAthlete() {
  }

  protected PersistentAthlete(
      final String name, final String firstName, final LocalDate birthday, final Gender gender) {
    super();
    this.name = name;
    this.firstname = firstName;
    this.birthday = birthday;
    this.gender = gender;
  }

  @Override
  public Long getId() {
    return this.id;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public String getFirstname() {
    return this.firstname;
  }

  @Override
  public void setFirstname(final String firstName) {
    this.firstname = firstName;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public LocalDate getBirthday() {
    return this.birthday;
  }

  @Override
  public void setBirthday(final LocalDate birthday) {
    this.birthday = birthday;
  }

  @Override
  public Gender getGender() {
    return this.gender;
  }

  @Override
  public void setGender(final Gender gender) {
    this.gender = gender;
  }

  @Override
  public String toString() {
    return this.firstname + " " + this.name;
  }

  @Override
  public List<AthleteRegistration> getRegistrations() {
    return null;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof PersistentAthlete
        && ((PersistentAthlete) obj).id == this.id;
  }
}
