package org.bitbucket.nwalter.scoreboard.api.persistence.event;

import org.bitbucket.nwalter.scoreboard.api.competition.Competition;
import org.bitbucket.nwalter.scoreboard.api.event.Event;
import org.bitbucket.nwalter.scoreboard.api.event.EventScore;
import org.bitbucket.nwalter.scoreboard.api.event.EventType;
import org.bitbucket.nwalter.scoreboard.api.persistence.competition.PersistentCompetition;
import org.bitbucket.nwalter.scoreboard.utils.IterableUtils;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"competition_id", "name" }))
public class PersistentEvent implements Event {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String name;

  @ManyToOne
  @JoinColumn(name = "competition_id")
  private PersistentCompetition competition;

  @OneToMany(mappedBy = "event")
  private final Collection<PersistentEventScore> scores = new ArrayList<>();

  protected PersistentEvent() {  }

  protected PersistentEvent(final String name, final PersistentCompetition competition) {
    super();
    this.name = name;
    this.competition = competition;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public Long getId() {
    return this.id;
  }

  @Override
  public EventType<? extends EventScore> getType() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Competition getCompetition() {
    return this.competition;
  }

  @Override
  public Collection<EventScore> getScores() {
    return IterableUtils.castCollection(this.scores);
  }

}
