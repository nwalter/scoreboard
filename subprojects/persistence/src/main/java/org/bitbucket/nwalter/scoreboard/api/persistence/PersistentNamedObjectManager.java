package org.bitbucket.nwalter.scoreboard.api.persistence;

import org.bitbucket.nwalter.scoreboard.api.Named;
import org.bitbucket.nwalter.scoreboard.api.NamedObjectCollection;
import org.bitbucket.nwalter.scoreboard.utils.IterableUtils;

import com.google.common.base.Optional;
import com.google.common.collect.Iterables;

public class PersistentNamedObjectManager<T extends Named, P extends T> implements NamedObjectCollection<T> {

  private final CrudNamedObjectRepository<P, Long> repository;

  public PersistentNamedObjectManager(final CrudNamedObjectRepository<P, Long> repository) {
    super();
    this.repository = repository;
  }

  @Override
  public T getByName(final String name) {
    return Iterables.getOnlyElement(this.repository.findByName(name));
  }

  @Override
  public Optional<T> findByName(final String name) {
    return IterableUtils.getOnlyElement(this.repository.findByName(name));
  }

  @SuppressWarnings("unchecked")
  @Override
  public Iterable<T> getAll() {
    return  (Iterable<T>) this.repository.findAll();
  }


}
