package org.bitbucket.nwalter.scoreboard.api.persistence;

import org.bitbucket.nwalter.scoreboard.api.Named;

import java.util.List;

public interface NamedObjectRepository<T extends Named> {

  List<T> findByName(String name);
}
