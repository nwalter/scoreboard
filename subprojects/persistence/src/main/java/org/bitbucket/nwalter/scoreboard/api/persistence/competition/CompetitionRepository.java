package org.bitbucket.nwalter.scoreboard.api.persistence.competition;

import org.bitbucket.nwalter.scoreboard.api.persistence.CrudNamedObjectRepository;

public interface CompetitionRepository extends CrudNamedObjectRepository<PersistentCompetition, Long> {
}
