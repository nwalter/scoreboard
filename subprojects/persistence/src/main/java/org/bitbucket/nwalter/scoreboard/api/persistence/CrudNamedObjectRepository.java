package org.bitbucket.nwalter.scoreboard.api.persistence;

import org.bitbucket.nwalter.scoreboard.api.Named;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface CrudNamedObjectRepository<T extends Named, ID extends Serializable> extends
NamedObjectRepository<T>, CrudRepository<T, ID> {

}
