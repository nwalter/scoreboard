package org.bitbucket.nwalter.scoreboard.api.persistence.event;

import org.bitbucket.nwalter.scoreboard.api.event.Event;
import org.bitbucket.nwalter.scoreboard.api.event.EventScore;
import org.bitbucket.nwalter.scoreboard.api.persistence.registration.PersistentAthleteRegistration;
import org.bitbucket.nwalter.scoreboard.api.registration.AthleteRegistration;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {
    "registration_id", "event_id"
}))
public class PersistentEventScore implements EventScore {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private int score;

  @ManyToOne
  @JoinColumn(name = "registration_id")
  private PersistentAthleteRegistration registration;

  @ManyToOne
  @JoinColumn(name = "event_id")
  private PersistentEvent event;

  protected PersistentEventScore() {
  }

  protected PersistentEventScore(final int score, final PersistentAthleteRegistration registration,
      final PersistentEvent event) {
    super();
    this.score = score;
    this.registration = registration;
    this.event = event;
  }

  @Override
  public AthleteRegistration getRegistration() {
    return this.registration;
  }

  @Override
  public Event getEvent() {
    return this.event;
  }

  @Override
  public int getScore() {
    return this.score;
  }
}
