package org.bitbucket.nwalter.scoreboard.api;

public interface PersistentObject {
  Long getId();
}
