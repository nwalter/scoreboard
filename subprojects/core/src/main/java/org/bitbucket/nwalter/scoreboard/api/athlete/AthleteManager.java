package org.bitbucket.nwalter.scoreboard.api.athlete;

import org.bitbucket.nwalter.scoreboard.api.NamedObjectCollection;
import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete.Gender;

import java.time.LocalDate;
import java.util.List;

public interface AthleteManager extends NamedObjectCollection<Athlete> {

  Athlete create(String name, String firstName, LocalDate birthday, Gender gender);

  Athlete save(Athlete athlete);

  List<Athlete> searchByName(String query);

  Athlete getById(Long id);

}
