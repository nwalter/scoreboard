package org.bitbucket.nwalter.scoreboard.api.registration;

import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete;
import org.bitbucket.nwalter.scoreboard.api.competition.Competition;

public interface AthleteRegistrationManager {

  AthleteRegistration createRegistration(Athlete athlete, Competition competition);

}
