package org.bitbucket.nwalter.scoreboard.utils;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;

import java.util.Collection;
import java.util.List;

public class IterableUtils {

  public static <T> Optional<T> getOnlyElement(final Iterable<? extends T> iterable) {
    final T element = Iterables.getOnlyElement(iterable, null);
    return Optional.fromNullable(element);
  }

  /**
   * Coerces the provided iterable to the desired supertype.
   */
  public static <R, T extends R> Iterable<R> coerce(final Iterable<T> iterable) {
    return Iterables.transform(iterable, new Function<T,R>() {

      @Override
      public R apply(final T input) {
        return input;
      }

    });
  }

  public static <T, R> Iterable<R> cast(final Iterable<T> iterable, final Class<R> cls) {
    return Iterables.transform(iterable, cast(cls));
  }

  /**
   * Returns a function that casts to the provides class.
   */
  public static <R, T> Function<T, R> cast(final Class<R> cls) {
    return new Function<T, R>() {
      @SuppressWarnings("unchecked")
      @Override
      public R apply(final T input) {
        return (R) input;
      }
    };
  }

  @SuppressWarnings("unchecked")
  public static <T extends R, R> List<R> castList(final List<T> list) {
    return (List<R>) list;
  }

  @SuppressWarnings("unchecked")
  public static <T> Collection<T> castCollection(final Collection<? extends T> collection) {
    return (Collection<T>) collection;
  }
}
