package org.bitbucket.nwalter.scoreboard.api;

public interface Named {

  String getName();

  void setName(String name);
}
