package org.bitbucket.nwalter.scoreboard.api;

import com.google.common.base.Optional;

public interface NamedObjectCollection<T extends Named> {

  T getByName(String name);

  Optional<T> findByName(String name);

  Iterable<T> getAll();
}
