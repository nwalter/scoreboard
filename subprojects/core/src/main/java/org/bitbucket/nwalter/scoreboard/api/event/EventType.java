package org.bitbucket.nwalter.scoreboard.api.event;

import java.util.Comparator;

public interface EventType<T extends EventScore> extends EventScoreFactory<T>, Comparator<T> {


}
