package org.bitbucket.nwalter.scoreboard.api.registration;

import org.bitbucket.nwalter.scoreboard.api.PersistentObject;
import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete;
import org.bitbucket.nwalter.scoreboard.api.competition.Competition;

public interface AthleteRegistration extends PersistentObject {

  Athlete getAthlete();

  Competition getCompetition();

}
