package org.bitbucket.nwalter.scoreboard.api.event;

public interface EventScoreFactory<T extends EventScore> {

  T createScore();

}
