package org.bitbucket.nwalter.scoreboard.api.athlete;

import org.bitbucket.nwalter.scoreboard.api.Named;
import org.bitbucket.nwalter.scoreboard.api.PersistentObject;
import org.bitbucket.nwalter.scoreboard.api.registration.AthleteRegistration;

import java.time.LocalDate;
import java.util.List;

public interface Athlete extends Named, PersistentObject {

  String getFirstname();

  void setFirstname(String firstName);

  LocalDate getBirthday();

  void setBirthday(LocalDate birthday);

  Gender getGender();

  void setGender(Gender gender);

  List<AthleteRegistration> getRegistrations();

  public enum Gender {
    MALE,
    FEMALE;
  }
}
