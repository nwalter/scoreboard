package org.bitbucket.nwalter.scoreboard.api.event;

import org.bitbucket.nwalter.scoreboard.api.registration.AthleteRegistration;

public interface EventScore {

  AthleteRegistration getRegistration();

  Event getEvent();

  int getScore();
}
