package org.bitbucket.nwalter.scoreboard.api.competition;

import org.bitbucket.nwalter.scoreboard.api.NamedObjectCollection;

public interface CompetitionManager extends NamedObjectCollection<Competition> {

  Competition create(String name);

  Competition getById(long id);

  void save(Competition competition);
}
