package org.bitbucket.nwalter.scoreboard.api.event;

import org.bitbucket.nwalter.scoreboard.api.Named;
import org.bitbucket.nwalter.scoreboard.api.PersistentObject;
import org.bitbucket.nwalter.scoreboard.api.competition.Competition;

import java.util.Collection;


public interface Event extends Named, PersistentObject {

  EventType<? extends EventScore> getType();

  Competition getCompetition();

  Collection<EventScore> getScores();

}
