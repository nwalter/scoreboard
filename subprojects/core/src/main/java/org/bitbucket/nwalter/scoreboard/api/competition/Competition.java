package org.bitbucket.nwalter.scoreboard.api.competition;

import org.bitbucket.nwalter.scoreboard.api.Named;
import org.bitbucket.nwalter.scoreboard.api.PersistentObject;
import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete;
import org.bitbucket.nwalter.scoreboard.api.event.Event;
import org.bitbucket.nwalter.scoreboard.api.registration.AthleteRegistration;

import java.util.Collection;
import java.util.List;

public interface Competition extends Named, PersistentObject {

  List<Event> getEvents();

  Collection<AthleteRegistration> getAthletes();

  void addRegistration(AthleteRegistration registration);

  boolean isRegistered(Athlete athlete);
}
