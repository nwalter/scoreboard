package org.bitbucket.nwalter.scoreboard.views;

import org.bitbucket.nwalter.scoreboard.api.registration.AthleteRegistration;
import org.bitbucket.nwalter.scoreboard.window.RegisterAthleteWindow;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SuppressWarnings("serial")
@SpringView(name = CompetitionRegistrationView.VIEW_NAME)
public class CompetitionRegistrationView extends AbstractCompetitionView {

  public static final String VIEW_NAME = "registrations";

  private Table registrationTable;
  private BeanItemContainer<AthleteRegistration> registrationContainer;
  private VerticalLayout root;

  @Autowired
  private RegisterAthleteWindow registerAthleteWindow;

  @PostConstruct
  protected void init() {
    this.root = new VerticalLayout();
    this.root.setMargin(true);
    this.setCompositionRoot(this.root);

    final MenuBar menuBar = new MenuBar();
    menuBar.addItem("Register athlete", selectedItem -> {
      this.getUI().addWindow(this.registerAthleteWindow);
      this.registerAthleteWindow.center();
      // TODO Athlete register window
    });
    this.root.addComponent(menuBar);

    this.registrationContainer = new BeanItemContainer<>(AthleteRegistration.class);
    this.registrationContainer.addNestedContainerBean("athlete");

    this.registrationTable = new Table("<h2>Registrations</h2>");
    this.registrationTable.setCaptionAsHtml(true);
    this.registrationTable.setContainerDataSource(this.registrationContainer);
    this.registrationTable.setVisibleColumns("athlete.name", "athlete.firstname", "athlete.gender");
    this.registrationTable.setColumnHeaders("Name", "Firstname", "Gender");

    this.root.addComponent(this.registrationTable);
  }

  @Override
  public void onEnter(final ViewChangeEvent event) {
    this.registrationContainer.addAll(this.getCompetition().getAthletes());
    this.registerAthleteWindow.setCompetition(this.getCompetition());
    this.registerAthleteWindow.setRegistrationContainer(this.registrationContainer);
  }

}
