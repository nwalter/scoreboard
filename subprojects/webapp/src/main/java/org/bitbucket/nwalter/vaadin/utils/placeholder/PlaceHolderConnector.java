package org.bitbucket.nwalter.vaadin.utils.placeholder;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.vaadin.client.ComponentConnector;
import com.vaadin.client.ServerConnector;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.extensions.AbstractExtensionConnector;
import com.vaadin.client.ui.VTextField;
import com.vaadin.shared.ui.Connect;

@Connect(PlaceHolder.class)
public class PlaceHolderConnector extends AbstractExtensionConnector {

  private static final long serialVersionUID = -3584902497101338136L;

  private VTextField textField;

  @Override
  protected void extend(final ServerConnector target) {
    target.addStateChangeHandler(new StateChangeEvent.StateChangeHandler() {

      private static final long serialVersionUID = -9044164134122415573L;

      @Override
      public void onStateChanged(final StateChangeEvent stateChangeEvent) {
        Scheduler.get().scheduleDeferred(new ScheduledCommand() {
          @Override
          public void execute() {
            PlaceHolderConnector.this.updatePlaceHolder();
          }
        });
      }
    });
    this.textField = (VTextField) ((ComponentConnector) target).getWidget();
  }


  @Override
  public PlaceHolderState getState() {
    return (PlaceHolderState) super.getState();
  }

  private void updatePlaceHolder() {
    this.textField.getElement().setAttribute("placeholder", this.getState().placeholder);
  }

}