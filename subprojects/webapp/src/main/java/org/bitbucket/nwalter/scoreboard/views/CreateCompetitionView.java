package org.bitbucket.nwalter.scoreboard.views;

import org.bitbucket.nwalter.scoreboard.api.competition.Competition;
import org.bitbucket.nwalter.scoreboard.api.competition.CompetitionManager;
import org.bitbucket.nwalter.vaadin.utils.OnEnterKeyHandler;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SpringView(name = CreateCompetitionView.VIEW_NAME)
public class CreateCompetitionView extends VerticalLayout implements View {

  public static final String VIEW_NAME = "create-competition";

  private static final long serialVersionUID = 4589500413557290971L;

  private TextField name;

  @Autowired
  private CompetitionManager competitionManager;

  @SuppressWarnings("serial")
  @PostConstruct
  protected void init() {
    this.setSizeFull();
    this.setMargin(true);

    final FormLayout form = new FormLayout();
    this.addComponent(form);
    form.setCaptionAsHtml(true);
    form.setCaption("<h1>Create a new competition</h1>");
    form.setSizeUndefined();

    this.name = new TextField("Name", "");
    this.name.setWidth(100.0f, Unit.PERCENTAGE);


    form.addComponent(this.name);

    final Label space = new Label();
    space.setHeight("30px");
    form.addComponent(space);
    final HorizontalLayout buttonBar = new HorizontalLayout();
    buttonBar.setMargin(true);
    buttonBar.setSpacing(true);
    form.addComponent(buttonBar);

    final Button abort = new Button("Abort");
    abort.addStyleName(ValoTheme.BUTTON_LINK);
    abort.addClickListener(new ClickListener() {

      @Override
      public void buttonClick(final ClickEvent event) {
        CreateCompetitionView.this.getUI().getNavigator().navigateTo("/");
      }
    });

    final Button createButton = new Button("Create");
    createButton.addStyleName(ValoTheme.BUTTON_PRIMARY);
    createButton.addClickListener(new ClickListener() {

      @Override
      public void buttonClick(final ClickEvent event) {
        CreateCompetitionView.this.createCompetition();
      }

    });
    buttonBar.addComponent(createButton);
    buttonBar.addComponent(abort);
    new OnEnterKeyHandler() {

      @Override
      public void onEnterKeyPressed() {
        createButton.click();
      }
    }.installOn(this.name);

  }

  private void createCompetition() {
    try {
      final Competition competition = this.competitionManager.create(this.name.getValue());
      Notification.show(String.format("Competition %s created successful", competition.getName()),
          Notification.Type.TRAY_NOTIFICATION);
      this.getUI().getNavigator().navigateTo(CompetitionsView.VIEW_NAME);
    } catch (final IllegalArgumentException e) {
      Notification.show(e.getLocalizedMessage(), Notification.Type.WARNING_MESSAGE);
    }
  }

  @Override
  public void enter(final ViewChangeEvent event) {
    this.name.focus();
  }
}
