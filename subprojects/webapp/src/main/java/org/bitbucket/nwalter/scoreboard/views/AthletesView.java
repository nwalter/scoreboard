package org.bitbucket.nwalter.scoreboard.views;

import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete;
import org.bitbucket.nwalter.scoreboard.api.athlete.AthleteManager;

import com.google.common.collect.FluentIterable;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.MouseEventDetails.MouseButton;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SuppressWarnings("serial")
@SpringView(name = AthletesView.VIEW_NAME)
public class AthletesView extends VerticalLayout implements View {

  public static final String VIEW_NAME = "athletes";

  @Autowired
  private AthleteManager athleteManager;

  @PostConstruct
  protected void init() {
    this.setMargin(true);

    final MenuBar menuBar = new MenuBar();
    menuBar.addItem("Add athlete", selectedItem -> {
      this.getUI().getNavigator().navigateTo(AddAthleteView.VIEW_NAME);
    });

    this.addComponent(menuBar);

    final BeanItemContainer<Athlete> athleteContainer = new BeanItemContainer<Athlete>(Athlete.class);
    athleteContainer.addAll(FluentIterable.from(this.athleteManager.getAll()).toList());

    final Table athleteTable = new Table("<h1>Athletes</h1>");
    athleteTable.setCaptionAsHtml(true);
    athleteTable.setWidth(100.0f, Unit.PERCENTAGE);
    athleteTable.setContainerDataSource(athleteContainer);
    athleteTable.setVisibleColumns("name", "firstname", "birthday", "gender");
    athleteTable.setColumnHeaders("Name", "Firstname", "Gender", "Birthday");
    athleteTable.addItemClickListener(event -> {
      if (event.getButton() == MouseButton.LEFT)  {
        // show athlete
      }
    });


    this.addComponent(athleteTable);
  }

  @Override
  public void enter(final ViewChangeEvent event) {
  }

}
