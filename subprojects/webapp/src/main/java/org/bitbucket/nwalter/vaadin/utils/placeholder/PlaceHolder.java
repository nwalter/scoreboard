package org.bitbucket.nwalter.vaadin.utils.placeholder;

import com.vaadin.server.AbstractClientConnector;
import com.vaadin.server.AbstractExtension;
import com.vaadin.server.ClientConnector;
import com.vaadin.ui.TextField;

public class PlaceHolder extends AbstractExtension {

  private static final long serialVersionUID = 681836884106440251L;

  private PlaceHolder() {
  }

  /**
   * Extends the text field with the placeholder.
   * Returns the placeholder for the text field.
   */
  public static PlaceHolder extend(final TextField textField) {
    final PlaceHolder placeHolder = new PlaceHolder();
    placeHolder.extend((AbstractClientConnector) textField);
    return placeHolder;
  }

  @Override
  protected Class<? extends ClientConnector> getSupportedParentType() {
    return TextField.class;
  }

  @Override
  protected PlaceHolderState getState() {
    return (PlaceHolderState) super.getState();
  }

  public void setPlaceholder(final String placeholder) {
    this.getState().placeholder = placeholder;
  }

}
