package org.bitbucket.nwalter.scoreboard.views;

import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@SuppressWarnings("serial")
@Component
@VaadinSessionScope
public class ApplicationSidebar extends CssLayout implements ViewChangeListener {

  private CssLayout menuItems;

  @PostConstruct
  public void init() {
    this.setPrimaryStyleName(ValoTheme.MENU_ROOT);
    this.setSizeUndefined();
  }

  private CssLayout buildMenuItems() {
    this.menuItems = new CssLayout();
    this.menuItems.setSizeUndefined();
    this.menuItems.setPrimaryStyleName(ValoTheme.MENU_PART);
    final Label manage = new Label("Manage");
    manage.setPrimaryStyleName(ValoTheme.MENU_SUBTITLE);
    this.menuItems.addComponent(manage);
    this.menuItems.addComponent(this.buildMenuItem("Competitions", CompetitionsView.VIEW_NAME));
    this.menuItems.addComponent(this.buildMenuItem("Athletes", AthletesView.VIEW_NAME));
    return this.menuItems;
  }

  /**
   * Creates a new Button for the sidebar.
   * Does not add the button to the sidebar!
   */
  public Button buildMenuItem(final String text, final String viewName) {
    final Button competitions = new Button(text);
    competitions.setPrimaryStyleName(ValoTheme.MENU_ITEM);
    competitions.addClickListener(new ClickListener() {

      @Override
      public void buttonClick(final ClickEvent event) {
        ApplicationSidebar.this.getUI().getNavigator().navigateTo(viewName);
      }
    });
    return competitions;
  }

  private com.vaadin.ui.Component buildTitle() {
    final Label logo = new Label("Score<strong>board</strong>",
        ContentMode.HTML);
    logo.setSizeUndefined();
    final HorizontalLayout logoWrapper = new HorizontalLayout(logo);
    logoWrapper.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
    logoWrapper.addStyleName(ValoTheme.MENU_TITLE);
    return logoWrapper;
  }

  public void addPart(final com.vaadin.ui.Component customMenuPart) {
    this.menuItems.addComponent(customMenuPart);
  }

  @Override
  public boolean beforeViewChange(final ViewChangeEvent event) {
    this.removeAllComponents();
    this.addComponent(this.buildTitle());
    this.addComponent(this.buildMenuItems());
    return true;
  }

  @Override
  public void afterViewChange(final ViewChangeEvent event) {
  }

}
