package org.bitbucket.nwalter.scoreboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScoreboardApplication {

  public static void main(final String[] args) {
    SpringApplication.run(ScoreboardApplication.class, args);
  }
}
