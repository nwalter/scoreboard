package org.bitbucket.nwalter.scoreboard.window;

import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete;
import org.bitbucket.nwalter.scoreboard.api.athlete.AthleteManager;
import org.bitbucket.nwalter.scoreboard.api.competition.Competition;
import org.bitbucket.nwalter.scoreboard.api.competition.CompetitionManager;
import org.bitbucket.nwalter.scoreboard.api.registration.AthleteRegistration;
import org.bitbucket.nwalter.scoreboard.api.registration.AthleteRegistrationManager;

import com.google.common.base.Splitter;
import com.google.common.collect.Sets;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.shared.ui.window.WindowMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.suggestfield.SuggestField;
import org.vaadin.suggestfield.SuggestField.SuggestionConverter;
import org.vaadin.suggestfield.SuggestField.SuggestionHandler;
import org.vaadin.suggestfield.client.SuggestFieldSuggestion;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

@SuppressWarnings("serial")
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RegisterAthleteWindow extends Window {

  private Competition competition;

  @Autowired
  private AthleteManager athleteManager;

  @Autowired
  private CompetitionManager competitionManager;

  @Autowired
  private AthleteRegistrationManager athleteRegistrationManager;

  private BeanItemContainer<AthleteRegistration> registrations;

  private CheckBox registerFurther;

  private SuggestField athleteField;

  @PostConstruct
  protected void init() {
    this.setCaptionAsHtml(true);
    this.setCaption("<h2>Register athlete</h2>");
    this.setSizeUndefined();
    this.setModal(true);
    this.setWindowMode(WindowMode.NORMAL);
    this.setDraggable(false);
    this.setWidth(300.0f, Unit.PIXELS);

    final VerticalLayout content = new VerticalLayout();
    content.setMargin(true);
    this.setContent(content);

    this.athleteField = this.createAthleteField();

    content.addComponent(this.athleteField);
    content.addComponent(new Label());
    content.addComponent(this.createButtonBar());
  }

  private HorizontalLayout createButtonBar() {
    this.registerFurther = new CheckBox("Register further athlete");

    final Button registerButton = new Button("Register", event -> {
      final Athlete athlete = (Athlete) this.athleteField.getValue();
      final AthleteRegistration athleteRegistration =
          this.athleteRegistrationManager.createRegistration(athlete, this.competition);
      this.competitionManager.save(this.competition);
      this.registrations.addBean(athleteRegistration);

      this.athleteField.clear();

      if (!this.registerFurther.getValue()) {
        this.close();
      }
    });
    registerButton.addStyleName(ValoTheme.BUTTON_PRIMARY);

    final Button closeButton = new Button("Close", event -> {
      this.close();
    });
    closeButton.addStyleName(ValoTheme.BUTTON_LINK);

    final HorizontalLayout bar = new HorizontalLayout(
        registerButton,
        closeButton,
        this.registerFurther);
    bar.setSpacing(true);
    return bar;
  }

  private SuggestField createAthleteField() {
    final SuggestField field = new SuggestField();
    field.setCaption("Athlete");
    field.setNewItemsAllowed(false);
    field.setRequired(true);
    field.setMinimumQueryCharacters(1);
    field.setSuggestionHandler(new SuggestionHandler() {

      @Override
      public List<Object> searchItems(final String query) {
        return RegisterAthleteWindow.this.searchAthlete(query);
      }

    });
    field.setSuggestionConverter(new SuggestionConverter() {

      @Override
      public SuggestFieldSuggestion toSuggestion(final Object item) {
        if (item != null) {
          final Athlete athlete = (Athlete) item;
          return new SuggestFieldSuggestion(athlete.getId().toString(), athlete.toString(), athlete.toString());
        }
        return null;
      }

      @Override
      public Object toItem(final SuggestFieldSuggestion suggestion) {
        final Long id = Long.valueOf(suggestion.getId());
        return RegisterAthleteWindow.this.athleteManager.getById(id) ;
      }
    });
    return field;
  }

  public void setCompetition(final Competition competition) {
    this.competition = competition;
  }

  public void setRegistrationContainer(final BeanItemContainer<AthleteRegistration> registrations) {
    this.registrations = registrations;
  }


  private List<Object> searchAthlete(final String query) {
    final List<String> parts = Splitter.onPattern("\\s+").splitToList(query);


    final Optional<Set<Athlete>> athletes = parts.stream()
        .map(this.athleteManager::searchByName)
        .<Set<Athlete>>map(Sets::newHashSet).reduce(
            (that, other) -> Sets.intersection(that, other));
    if (athletes.isPresent()) {
      return athletes.get().stream().collect(Collectors.toList());
    } else {
      return java.util.Collections.emptyList();
    }
  }
}
