package org.bitbucket.nwalter.scoreboard.views;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import javax.annotation.PostConstruct;

@SuppressWarnings("serial")
@SpringView(name = CompetitionView.VIEW_NAME)
public class CompetitionView extends AbstractCompetitionView {

  public static final String VIEW_NAME = "competition";

  private VerticalLayout root;
  private Label nameLabel;

  @PostConstruct
  protected void init() {
    this.root = new VerticalLayout();
    this.setCompositionRoot(this.root);

    this.nameLabel = new Label();
    this.nameLabel.setContentMode(ContentMode.HTML);

    this.root.addComponent(this.nameLabel);
    this.root.setMargin(true);
  }

  @Override
  public void onEnter(final ViewChangeEvent event) {
    this.nameLabel.setValue(String.format("<h1>Competition: %s</h1>", this.getCompetition().getName()));
  }
}
