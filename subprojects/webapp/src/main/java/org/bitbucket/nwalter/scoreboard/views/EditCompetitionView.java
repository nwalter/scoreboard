package org.bitbucket.nwalter.scoreboard.views;

import org.bitbucket.nwalter.scoreboard.api.competition.Competition;
import org.bitbucket.nwalter.scoreboard.api.registration.AthleteRegistration;

import com.google.common.collect.FluentIterable;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitEvent;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.fieldgroup.FieldGroup.CommitHandler;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import javax.annotation.PostConstruct;

@SuppressWarnings("serial")
@SpringView(name = EditCompetitionView.VIEW_NAME)
public class EditCompetitionView extends AbstractCompetitionView implements View {

  public static final String VIEW_NAME = "edit-competition";

  private BeanFieldGroup<Competition> group;

  private BeanItemContainer<AthleteRegistration> athletesContainer;

  private VerticalLayout root;

  @PostConstruct
  protected void init() {
    this.root = new VerticalLayout();
    this.root.setMargin(true);
    this.setCompositionRoot(this.root);

    final Label caption = new Label("<h1>Edit competition</h1>", ContentMode.HTML);
    this.root.addComponent(caption);
    this.athletesContainer = new BeanItemContainer<AthleteRegistration>(AthleteRegistration.class);
    this.athletesContainer.addNestedContainerProperty("athlete.name");

    this.group = new BeanFieldGroup<>(Competition.class);
    this.group.addCommitHandler(new CommitHandler() {

      private static final long serialVersionUID = 764656499747996867L;

      @Override
      public void preCommit(final CommitEvent commitEvent) throws CommitException {}

      @Override
      public void postCommit(final CommitEvent commitEvent) throws CommitException {
        try {
          final Competition competition = EditCompetitionView.this.group.getItemDataSource().getBean();
          EditCompetitionView.this.getCompetitionsManager().save(competition);
          Notification.show(String.format("Compoetion %s saved", competition.getName()),
              Type.TRAY_NOTIFICATION);
          EditCompetitionView.this.getUI().getNavigator().navigateTo(
              CompetitionView.VIEW_NAME + getPath(EditCompetitionView.this.getCompetition()));
        } catch (final Exception e) {
          throw new CommitException(e);
        }
      }
    });

    final FormLayout form = new FormLayout();
    final TextField nameField = new TextField("Name");

    this.group.bind(nameField, "name");
    form.addComponent(nameField);

    this.root.addComponent(form);

    final Button saveButton = new Button("Save", event -> {
      try {
        EditCompetitionView.this.group.commit();
      } catch (final CommitException e) {
        Notification.show(e.getLocalizedMessage(), Type.ERROR_MESSAGE);
      }
    });
    saveButton.addStyleName(ValoTheme.BUTTON_PRIMARY);

    final Button abortButton = new Button("Abort", event -> {
      UI.getCurrent().getNavigator().navigateTo(CompetitionView.VIEW_NAME + getPath(this.getCompetition()));
    });
    abortButton.addStyleName(ValoTheme.BUTTON_LINK);

    this.root.addComponent(new HorizontalLayout(saveButton, abortButton));

  }

  @Override
  public void onEnter(final ViewChangeEvent event) {
    this.group.setItemDataSource(this.getCompetition());
    this.athletesContainer.addAll(FluentIterable.from(this.getCompetition().getAthletes()).toList());
  }
}
