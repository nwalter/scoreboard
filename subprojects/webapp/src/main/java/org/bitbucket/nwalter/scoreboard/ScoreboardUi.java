package org.bitbucket.nwalter.scoreboard;

import org.bitbucket.nwalter.scoreboard.views.ApplicationSidebar;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

@Theme("scoreboard")
@Widgetset("org.bitbucket.nwalter.scoreboard.ScoreboardWidgetSet")
@SpringUI
public class ScoreboardUi extends UI {

  private static final long serialVersionUID = -4338047978706618983L;

  @Autowired
  private SpringViewProvider viewProvider;

  @Autowired
  private ApplicationSidebar sidebar;

  @Override
  protected void init(final VaadinRequest request) {
    Responsive.makeResponsive(this);
    this.setPrimaryStyleName(ValoTheme.UI_WITH_MENU);

    final HorizontalLayout root = new HorizontalLayout();
    root.setSizeFull();
    this.setContent(root);

    root.addComponent(this.sidebar);

    final ComponentContainer viewContainer = new CssLayout();
    viewContainer.setSizeFull();
    root.addComponent(viewContainer);
    root.setExpandRatio(viewContainer, 1.0f);

    final Navigator navigator = new Navigator(this, viewContainer);
    navigator.addProvider(this.viewProvider);
    navigator.addViewChangeListener(this.sidebar);
  }
}
