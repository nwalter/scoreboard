package org.bitbucket.nwalter.scoreboard.views;

import org.bitbucket.nwalter.scoreboard.api.competition.Competition;
import org.bitbucket.nwalter.scoreboard.api.competition.CompetitionManager;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.vaadin.data.util.BeanItem;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.NoSuchElementException;


@SuppressWarnings("serial")
public abstract class AbstractCompetitionView extends CustomComponent implements View {

  public static final String COMPETITION_PARAMETER_NAME = "competitionId";

  @Autowired
  private CompetitionManager competitionManager;

  private Competition competition;

  private BeanItem<Competition> competitionItem;

  @Autowired
  private ApplicationSidebar sidebar;

  public static String getPath(final Competition competition) {
    return "/" + COMPETITION_PARAMETER_NAME + "=" + competition.getId().toString();
  }

  public Competition getCompetition() {
    return this.competition;
  }

  public BeanItem<Competition> getCompetitionItem() {
    return this.competitionItem;
  }

  public CompetitionManager getCompetitionsManager() {
    return this.competitionManager;
  }

  public ApplicationSidebar getSidebar() {
    return this.sidebar;
  }

  @Override
  public void enter(final ViewChangeEvent event) {
    if (!Strings.isNullOrEmpty(event.getParameters())) {
      final Map<String, String> parameters = Splitter.on('/').withKeyValueSeparator("=").split(event.getParameters());

      final String competitionIdString = parameters.get(COMPETITION_PARAMETER_NAME);
      if (!Strings.isNullOrEmpty(competitionIdString)) {
        final long competitionId = Long.parseLong(competitionIdString);
        this.competition = this.competitionManager.getById(competitionId);
        this.competitionItem = new BeanItem<Competition>(this.competition, Competition.class);
        if (this.competition == null) {
          throw new NoSuchElementException("No competition with id " + competitionIdString);
        }

        this.populateSidebar();

        this.onEnter(event);
        return;
      }
    }
    throw new IllegalArgumentException("competition id is missing");
  }

  private void populateSidebar() {
    final Label sidebarLabel = new Label("Competition " + this.getCompetition().getName());
    sidebarLabel.addStyleName(ValoTheme.MENU_SUBTITLE);
    this.sidebar.addPart(sidebarLabel);
    final Button editButton =
        this.sidebar.buildMenuItem("Edit", EditCompetitionView.VIEW_NAME + getPath(this.competition));
    this.sidebar.addPart(editButton);
    final Button eventsButton = this.sidebar.buildMenuItem("Registrations",
        CompetitionRegistrationView.VIEW_NAME + getPath(this.competition)); // TODO
    this.sidebar.addPart(eventsButton);
  }

  public abstract void onEnter(final ViewChangeEvent event);
}
