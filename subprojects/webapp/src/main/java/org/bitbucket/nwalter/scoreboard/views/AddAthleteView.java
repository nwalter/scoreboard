package org.bitbucket.nwalter.scoreboard.views;

import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete;
import org.bitbucket.nwalter.scoreboard.api.athlete.Athlete.Gender;
import org.bitbucket.nwalter.scoreboard.api.athlete.AthleteManager;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitEvent;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.fieldgroup.FieldGroup.CommitHandler;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

import javax.annotation.PostConstruct;

@SpringView(name = AddAthleteView.VIEW_NAME)
@SuppressWarnings("serial")
public class AddAthleteView extends VerticalLayout implements View {

  public static final String VIEW_NAME = "add-athlete";

  private final class LocalDateConverter implements Converter<Date, LocalDate> {
    @Override
    public LocalDate convertToModel(final Date value, final Class<? extends LocalDate> targetType,
        final Locale locale) throws com.vaadin.data.util.converter.Converter.ConversionException {
      return LocalDateTime.ofInstant(Instant.ofEpochMilli(value.getTime()),
          ZoneId.systemDefault()).toLocalDate();
    }

    @Override
    public Date convertToPresentation(final LocalDate value, final Class<? extends Date> targetType,
        final Locale locale) throws com.vaadin.data.util.converter.Converter.ConversionException {
      final Instant instant = value.atStartOfDay(ZoneId.systemDefault()).toInstant();
      return Date.from(instant);
    }

    @Override
    public Class<LocalDate> getModelType() {
      return LocalDate.class;
    }

    @Override
    public Class<Date> getPresentationType() {
      return Date.class;
    }
  }

  private static final String GENDER_PROPERTY = "gender";

  private static final String BIRTHDAY = "birthday";

  private static final String FIRST_NAME_PROPERTY = "firstName";

  private static final String NAME_PROPERTY = "name";


  @Autowired
  private AthleteManager athleteManager;

  private TextField nameTextField;

  @PostConstruct
  protected void init() {
    this.setMargin(true);

    final Label heading =  new Label();
    heading.setCaptionAsHtml(true);
    heading.setCaption("<h1>Add athlete</h1>");
    this.addComponent(heading);

    final PropertysetItem item = new PropertysetItem();
    item.addItemProperty(NAME_PROPERTY, new ObjectProperty<>(""));
    item.addItemProperty(FIRST_NAME_PROPERTY, new ObjectProperty<>(""));

    item.addItemProperty(BIRTHDAY, new ObjectProperty<LocalDate>(LocalDate.of(1970, 1, 1)));
    item.addItemProperty(GENDER_PROPERTY, new ObjectProperty<>(Gender.MALE));

    this.nameTextField = new TextField("Name");
    this.nameTextField.setRequired(true);

    final TextField firstNameTextField = new TextField("Firstname");
    firstNameTextField.setRequired(true);

    final DateField birthdayDateField = new PopupDateField("Birthday");
    birthdayDateField.setConverter(new LocalDateConverter());
    birthdayDateField.setRequired(true);
    birthdayDateField.setDateFormat("dd.MM.yyyy");

    final ComboBox genderComboBox = new ComboBox("Gender");
    genderComboBox.setRequired(true);
    genderComboBox.addItem(Gender.MALE);
    genderComboBox.addItem(Gender.FEMALE);

    final FieldGroup fieldGroup = new FieldGroup();
    fieldGroup.bind(this.nameTextField, NAME_PROPERTY);
    fieldGroup.bind(firstNameTextField, FIRST_NAME_PROPERTY);
    fieldGroup.bind(genderComboBox, GENDER_PROPERTY);
    fieldGroup.bind(birthdayDateField, BIRTHDAY);
    this.addComponent(this.nameTextField);
    this.addComponent(firstNameTextField);
    this.addComponent(birthdayDateField);
    this.addComponent(genderComboBox);
    fieldGroup.setItemDataSource(item);

    final Button abortButton = new Button("Abort");
    abortButton.addStyleName(ValoTheme.BUTTON_LINK);
    abortButton.addClickListener(event -> {
      this.getUI().getNavigator().navigateTo(AthletesView.VIEW_NAME);
    });

    final Button saveButton = new Button("Save");
    saveButton.addStyleName(ValoTheme.BUTTON_PRIMARY);
    saveButton.addClickListener(event -> {
      try {
        fieldGroup.commit();
      } catch (final Exception e) {
        Notification.show(e.getLocalizedMessage(), Type.ERROR_MESSAGE);
      }
    });
    final CheckBox addFurtherAthlete = new CheckBox("Add further athlete");

    final HorizontalLayout buttons = new HorizontalLayout(saveButton, abortButton);
    buttons.setSizeUndefined();
    buttons.setMargin(true);
    this.addComponent(buttons);
    this.addComponent(addFurtherAthlete);

    fieldGroup.addCommitHandler(new CommitHandler() {

      @Override
      public void preCommit(final CommitEvent commitEvent) throws CommitException {
      }

      @Override
      public void postCommit(final CommitEvent commitEvent) throws CommitException {

        final Item item = commitEvent.getFieldBinder().getItemDataSource();

        final Athlete athlete = AddAthleteView.this.createAthlete(item);
        Notification.show("Athlete "
            + athlete.getFirstname()
            + " "
            + athlete.getName()
            + " added",
            Type.TRAY_NOTIFICATION);
        if (addFurtherAthlete.getValue()) {
          fieldGroup.clear();
          AddAthleteView.this.nameTextField.focus();

        } else {
          AddAthleteView.this.getUI().getNavigator().navigateTo(AthletesView.VIEW_NAME);
        }
      }
    });
    this.nameTextField.focus();
  }

  private Athlete createAthlete(final Item item) {
    final String name = (String) item.getItemProperty(NAME_PROPERTY).getValue();
    final String firstName = (String) item.getItemProperty(FIRST_NAME_PROPERTY).getValue();
    final Gender gender = (Gender) item.getItemProperty(GENDER_PROPERTY).getValue();
    final LocalDate birthday = (LocalDate) item.getItemProperty(BIRTHDAY).getValue();

    return this.athleteManager.create(name, firstName, birthday, gender);
  }

  @Override
  public void enter(final ViewChangeEvent event) {
    this.nameTextField.focus();
  }

}
