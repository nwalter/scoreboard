package org.bitbucket.nwalter.scoreboard.views;

import org.bitbucket.nwalter.scoreboard.api.competition.Competition;
import org.bitbucket.nwalter.scoreboard.api.competition.CompetitionManager;

import com.google.common.collect.FluentIterable;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.MouseEventDetails.MouseButton;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SpringView(name = CompetitionsView.VIEW_NAME)
@SuppressWarnings("serial")
public class CompetitionsView extends VerticalLayout implements View {

  public static final String VIEW_NAME = "";

  @Autowired
  private CompetitionManager competitionManager;

  @SuppressWarnings("unchecked")
  @PostConstruct
  protected void init() {
    this.setMargin(true);

    final MenuBar menuBar = new MenuBar();
    menuBar.addItem("Create", new Command() {

      @Override
      public void menuSelected(final MenuItem selectedItem) {
        CompetitionsView.this.getUI().getNavigator().navigateTo(CreateCompetitionView.VIEW_NAME);
      }
    });
    this.addComponent(menuBar);

    final BeanItemContainer<Competition> container =
        new BeanItemContainer<Competition>(Competition.class);
    container.addAll(FluentIterable.from(this.competitionManager.getAll()).toList());

    final Table table = new Table("<h1>Competitions</h1>");
    table.setCaptionAsHtml(true);
    table.setWidth(100.0f, Unit.PERCENTAGE);
    table.setContainerDataSource(container);
    table.setVisibleColumns("name");
    table.setColumnHeader("name", "Name");
    table.addItemClickListener(event -> {
      
      if (event.getButton() == MouseButton.LEFT)  {
        BeanItem<Competition> item = (BeanItem<Competition>) event.getItem();
        CompetitionsView.this.getUI().getNavigator().navigateTo(
            CompetitionView.VIEW_NAME + CompetitionView.getPath(item.getBean()));
      }
      
    });

    this.addComponent(table);
  }

  @Override
  public void enter(final ViewChangeEvent event) {
    event.getNavigator().getUI().getPage().setTitle("Competitions");
  }

}
