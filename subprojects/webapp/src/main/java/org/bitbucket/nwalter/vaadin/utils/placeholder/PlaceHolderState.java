package org.bitbucket.nwalter.vaadin.utils.placeholder;

import com.vaadin.shared.communication.SharedState;

public class PlaceHolderState extends SharedState {

  private static final long serialVersionUID = -4605534023461299669L;

  public String placeholder;

}
