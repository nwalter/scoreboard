package org.bitbucket.nwalter.vaadin.utils;

import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.ui.TextField;

public abstract class OnEnterKeyHandler {

  private final ShortcutListener shortcutListener =
      new ShortcutListener("EnterOnTextFieldShortcut", ShortcutAction.KeyCode.ENTER, null) {

    private static final long serialVersionUID = -4070145784652733721L;

    @Override
    public void handleAction(final Object sender, final Object target) {
      OnEnterKeyHandler.this.onEnterKeyPressed();
    }
  };

  /**
   * Installs the {@link OnEnterKeyHandler} on the textField.
   */
  public void installOn(final TextField textField) {

    textField.addFocusListener(new FocusListener() {

      private static final long serialVersionUID = 3763617347585158527L;

      @Override
      public void focus(final FocusEvent event) {
        textField.addShortcutListener(OnEnterKeyHandler.this.shortcutListener);
      }
    });

    textField.addBlurListener(new BlurListener() {

      private static final long serialVersionUID = 2897784400330161498L;

      @Override
      public void blur(final BlurEvent event) {
        textField.removeShortcutListener(OnEnterKeyHandler.this.shortcutListener);
      }
    });
  }

  public abstract void onEnterKeyPressed();
}
